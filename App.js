/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {StackNavigator,TabNavigator,TabBarBottom} from 'react-navigation';

import ZhuComponent from './src/component/ZhuComponent';
import ZiComponent from './src/component/ZiComponent';
import BaiComponent from './src/component/BaiComponent';
import JiaComponent from './src/component/JiaComponent';
import GuidePageComponent from './src/component/GuidePageComponent';
import BaiChildComponent from './src/component/BaiChildComponent';
import JiaChildComponent from './src/component/JiaChildComponent';
import ZiChildComponent from './src/component/ZiChildComponent';
import splashView from './src/component/splashView';
import {setSpText,scaleSize} from './src/utils/ScreenUtil';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  render() {
    return (
      <Apps/>
    );
  }
}

const Tab = TabNavigator(  
  {  
    zhu:{  
      screen:ZhuComponent,  
      navigationOptions:({navigation}) => ({  
        tabBarLabel:'诸',  
        tabBarIcon: ({ tintColor, focused }) => (  
          <Icon name="yin-yang" size={scaleSize(56)} color={focused?'#FFFFFF':'#000000'} />  
      )  
      }),  
    },  
  
    zi:{  
          screen:ZiComponent,  
          navigationOptions:({navigation}) => ({  
          tabBarLabel:'子',
          tabBarIcon: ({ tintColor, focused }) => (  
            <Icon name="account-circle" size={scaleSize(56)}  color={focused?'#FFFFFF':'#000000'}/>  
        )   
		
        }),  
      },

	百:{  
          screen:BaiComponent,  
          navigationOptions:({navigation}) => ({  
          tabBarLabel:'百',  
          tabBarIcon: ({ tintColor, focused }) => (  
            <Icon name="google-earth" size={scaleSize(56)} color={focused?'#FFFFFF':'#000000'}/>  
        )  
        }),  
      },  

	家:{  
          screen:JiaComponent,  
          navigationOptions:({navigation}) => ({  
          tabBarLabel:'家',  
          tabBarIcon: ({ tintColor, focused }) => (  
            <Icon name="bank" size={scaleSize(56)} color={focused?'#FFFFFF':'#000000'} />  
        )  
        }),  
      },  	  
    },
    {  
      tabBarComponent:TabBarBottom,  
      tabBarPosition:'bottom',  
      swipeEnabled:false,  
      animationEnabled:false,  
      lazy:true,  
      tabBarOptions:{  
        activeTintColor:'#FFFFFF',  
        inactiveTintColor:'#000000', 
        style:{backgroundColor:'#E22018'},  
        labelStyle: {  
          fontSize:15, // 文字大小  
          },  
      }  
        
    },
  ); 
const Apps = StackNavigator({
  splashView:{
    screen:splashView,
    navigationOptions: {
      header:null
   },
  },
   guide:{
	  screen:GuidePageComponent,
	  navigationOptions: {
         header:null
      },
	},
  tab:{screen:Tab,
	navigationOptions: {
         header:null
      },
  },
  baiChild:{screen:BaiChildComponent},
  jiaChild:{screen:JiaChildComponent},
  ziChild:{screen:ZiChildComponent},
},{
  initialRouteName:'splashView',
});
